const int BUTTON_PIN = 11;   // Pin del botón
const int BUZZER_PIN = 2;    // Pin del buzzer
const int TRIGGER_PIN = 10;  // Pin del trigger del sensor de ultrasonido
const int ECHO_PIN = 9;      // Pin del echo del sensor de ultrasonido

long duration;
double distance;
int buttonState = 0;
bool buzzerActive = false;

void setup() {
  Serial.begin(9600);  // Inicializar la comunicación serial
  pinMode(BUTTON_PIN, INPUT_PULLUP);
  pinMode(BUZZER_PIN, OUTPUT);
  pinMode(TRIGGER_PIN, OUTPUT);
  pinMode(ECHO_PIN, INPUT);
}

void loop() {
  // Medir la distancia con el sensor de ultrasonido
  digitalWrite(TRIGGER_PIN, LOW);
  delayMicroseconds(2);
  digitalWrite(TRIGGER_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIGGER_PIN, LOW);
  duration = pulseIn(ECHO_PIN, HIGH);
  distance = duration * 0.034 / 2;  // Calcular la distancia en cm

  Serial.println(distance);

  buttonState = digitalRead(BUTTON_PIN);

  // Si el botón se presiona, cambiar el estado del buzzer (activar o desactivar)
  if (buttonState == HIGH) {
    buzzerActive = !buzzerActive;  // Cambiar el estado del buzzer
    if (buzzerActive) {
      //digitalWrite(BUZZER_PIN, HIGH); // Encender el buzzer
    } else {
      digitalWrite(BUZZER_PIN, LOW);  // Apagar el buzzer
    }
  } else {
    // Si el sensor de distancia detecta un objeto a menos de 30 cm y el buzzer está activado, activar el buzzer
    if (distance < 30 && buzzerActive) {
      digitalWrite(BUZZER_PIN, HIGH);  // Encender el buzzer
    } else {
      digitalWrite(BUZZER_PIN, LOW);  // Apagar el buzzer en otros casos
    }
  }

  delay(100);
}